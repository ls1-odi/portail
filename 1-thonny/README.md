> # Prise en main de Thonny
> [Outils pour le développement informatique](../Readme.md) \
> 1re année de licence maths-info, Univ. Lille \
> septembre 2023 \
> [![](https://mirrors.creativecommons.org/presskit/icons/cc.png){width=20}![](https://mirrors.creativecommons.org/presskit/icons/by.png){width=20}](https://creativecommons.org/licenses/by/4.0/deed.fr)


# 1. Objectifs

Ce document vise à vous faire découvrir Thonny, l'environnement de programmation
dédié à Python que nous allons utiliser toute l'année, ainsi que des fonctionnalités
communes à de nombreux logiciels. Le début de ce 
document est une mise à jour de la page sur Thonny de :

Informatique — Support d'enseignement
1re année de licence, Univ. Lille, septembre-décembre 2020.
Équipe pédagogique, Formation en informatique de Lille,
Faculté des sciences et technologies, Université de Lille.
Philippe Marquet, Maude Pupin, avec le soutien de Laurent Noé, Pierre Allegraud, Patrice Thibaud, inspiré·es des riches versions antérieures de l'Équipe pédagogique.
Disponible sur <https://hal.science/hal-04131704>


Thonny est un environnement de développement intégré (IDE en anglais)
dédié au langage de programmation Python, particulièrement adapté 
à l'apprentissage du langage. Il facilite l'écriture, l'exécution et le débogage de code 
Python en proposant diverses fonctionnalités. Nous décrivons 
les principales dans cette page.

**{+pour aller plus loin+}** Sur les IDE lire
la mini-BD [Dis ... c'est quoi un Raccourci Clavier ?](https://arpinux.org/discestquoi/dcq/dcq-ide.pdf)

En deuxième année de licence (L2) informatique vous découvrirez d'autres
éditeurs de code tels que Emacs. 


# 2. Installation


Thonny est installé sur les ordinateurs des salles informatiques.

Sur votre ordinateur personnel, vous pouvez installer Thonny en le 
téléchargeant à partir de [thonny.org](https://thonny.org/)
(versions Windows, macOS, Linux). Il inclut une dernière version 
de Python (3.10.6 en septembre 2023).


# 3. Premier aperçu

Une [vidéo](https://pod.univ-lille.fr/video/20948-demo_thonnymp4/) 
présente les différentes fonctionnalités décrites ci-dessous, pour une
version de Thonny antérieure (2021).

Les copies d'écran du reste du support ont été prises avec la version 4.1.2
de Thonny et 3.1.0 du plug-in L1test.


## → Ouvrir Thonny


Au lancement de Thonny, vous pouvez choisir le langage des menus.
Une fois les paramétrages effectués, on obtient une vue telle que celle de cette
première figure :

![Vue de Thonny à son lancement](img/thonny-vide.png){width=50%}

Les différents panneaux proposés par Thonny interagissent à tout moment
avec l'interpréteur Python et entre eux afin d'accéder aux informations 
concernant le code qui a été exécuté.  
Un interpréteur est un outil qui lit du code informatique et
l'exécute, c'est-à-dire qu'il accomplit les instructions contenues dans ce
code.

Le panneau L1Test sera décrit dans l'UE Programmation.

## → Le panneau Console


La console (*shell* en anglais) permet de taper du code Python et de
visualiser directement le résultat de son exécution. Par exemple,
Python résout des calculs arithmétiques ou manipule des chaînes
de caractères (voir ci-dessous).

![Exécution d'expressions dans la console de Thonny](img/thonny-console-expressions.png){width=45%}

Pour utiliser la console, tapez du code Python à la suite de l'invite de
commande (le symbole `>>>`) puis la touche <kbd>Entrée</kbd>. Le résultat 
des expressions apparaît sur la ligne suivante.
Les instructions autres que des expressions n'ont pas de résultat visible, 
comme par exemple les affectations (voir ci-dessous).

![Exécution d'une affectation dans la console de Thonny](img/thonny-console-affectation.png){width=45%}

**{-à faire-}** Exécuter les expressions et instructions qui sont
dans l'illustration ci-dessus.

Dans la console, les flèches vers le haut et vers le bas permettent d'afficher
de nouveau le code exécuté précédemment. 


## → Le panneau Variables

Thonny permet de visualiser les variables connues suite à l'exécution 
de code : dans le menu `Affichage`, cocher `Variables`.
Un nouveau panneau apparaît : 

![Affichage du panneau Variables de Thonny](img/thonny-variables.png){width=50%} 

Ce panneau affiche les variables définies dans l'interpréteur Python,
en indiquant leur nom (`Name`) et leur valeur (`Value`). 

**{-à faire-}** 

- Ouvrir le panneau `Variables`,
- dans la console, utiliser les flèches pour afficher
de nouveau l'instruction d'assignation d'une valeur à la variable `prenom`,
- changer cette valeur, exécuter l'instruction puis observer les changements dans le panneau `Variables`.


## → Le panneau de script

### Écrire, enregistrer et exécuter un script

Le panneau qui porte le nom `<sans nom>` lors du premier lancement de 
Thonny est l'éditeur de code, c'est-à-dire la zone de saisie et de sauvegarde 
des programmes, appelés aussi scripts, écrits dans le langage Python. 

Nous pouvons exécuter (_run_ en anglais) un script Python à l'aide de 
trois méthodes différentes, qui donnent le même résultat :

* en cliquant sur le bouton vert avec une flèche : ![Le bouton Exécuter de Thonny](img/thonny-bouton-run.png),
* en choisissant le menu : `Exécuter > Exécuter le script courant`,
* en utilisant le raccourci clavier : <kbd>F5</kbd>

Une fenêtre s'ouvre, invitant à sauvegarder (enregistrer) le script dans un fichier.
Ce que nous faisons, par exemple sous le nom `premier_programme.py`.

![Exécution d'un script dans Thonny](img/thonny-script1.png){width=50%} 

Le panneau `Variables` a été mis à jour avec les variables
contenues dans le script. Celles-ci peuvent être utilisées dans la
console car elles sont connues de l'interpréteur.

![Utilisation, dans la console, de variables définies dans un script, via Thonny](img/thonny-script2.png){width=50%} 

**{-à faire-}** Rechercher les trois méthodes qui permettent d'afficher un
script vide, ainsi que celles qui permettent de l'enregistrer.

**{-à faire-}** 

- Se placer dans un onglet de script vide (nouveau),
- copier-coller le script ci-dessous dedans à l'aide des raccourcis clavier vus précédemment
(vous remarquerez qu'une étoile apparaît à côté de `<sans nom>`, indiquant que des modifications
ont été réalisées, mais pas enregistrées),
- enregistrer le script dans un fichier nommé `premier_script.py` (l'étoile disparaît),
- exécuter le script.

```{python}
# Équipe pédagogique de ODI
# Ce script réalise un calcul mathématique très compliqué ;)
# 09/2023

v1 = 3 + 4
v2 = 5
res = v1 * v2
```

Vous remarquerez que la console se vide et affiche seulement la _commande magique_
`%Run premier_script.py` qui signifie que le script a été exécuté.
Le script n'ayant pas d'instruction d'affichage, aucune autre information n'est
affichée. La valeur de la variable `res` (ou de toute autre variable) peut être visualisée dans le panneau
`Variables` ou consultée en exécutant l'expression `res` dans la console.

Pour information, une commande magique est toujours précédée d'un ou deux `%`.
Ces commandes sont souvent des instructions shell qui permettent de, par 
exemple, se déplacer dans les répertoires (dossiers) de votre ordinateur.

### En-tête de fichier de code

Les lignes commençant par le caractère `#` sont des lignes de commentaire,
c'est-à-dire qu'elles sont ignorées par l'interpréteur Python.
Elles peuvent contenir autre chose que du code. 
Dans le script que nous vous avons donné ci-dessus, nous avons commencé 
par des lignes de commentaire qui précisent le ou les auteur·es du script, une brève 
description de son contenu, ainsi que la date de la dernière version du script.  
Il s'agit d'un en-tête de fichier. Commencer systématiquement un script
ou un programme par un en-tête pour décrire le contenu du fichier, 
ainsi que ses auteurs et la date est une bonne pratique de programmation 
que nous vous demandons de prendre dès maintenant.


### Coloration syntaxique

Peut-être avez-vous remarqué que le code que vous écrivez dans Thonny 
est coloré. Il s'agit de la coloration syntaxique, c'est-à-dire l'ajout
de couleurs sur du texte brut pour permettre une meilleure lisibilité du code.
Les mots-clés de Python sont écrits dans une couleur particulière, différente
du nom des variables et les différents types de données ont chacun une couleur
qui leur est propre.

Thonny suppose qu'un fichier dont l'extension est `.py` contient du code Python.
L'extension d'un fichier est la partie de son nom qui suit le `.`. Elle
indique au système de fichiers quel est le type du fichier. Par exemple,
un fichier qui contient du texte brut quelconque porte l'extension `.txt`, 
un fichier  qui contient du code Python (qui est aussi su texte brut) 
porte l'extension `.py`, un fichier image peut porter différentes extension,
selon le format de l'image : `.png`, `.jpg`, *etc.* 
Lorsque vous faite un double clic sur un fichier , le logiciel qui est utilisé
pour l'ouvrir est choisi en fonction de l'extension du fichier.
 
**{-à faire-}** 

- Dans le navigateur de fichiers, dupliquer (copier-coller) le fichier 
`premier_script.py`,
- renommer le fichier dupliqué en `premier_script.txt`,
- ouvrir ce fichier avec Thonny pour constater qu'il n'y a plus de coloration syntaxique.


### Principales fonctionnalités des menus Fichier et Édition

Vous avez découvert plusieurs fonctionnalités présentes dans différents logiciels,
dont les éditeurs de texte, avec leurs raccourcis clavier. Le tableau ci-dessous les reprend
et les complète :

| Raccourci                       | Effet                                                        |
|---------------------------------|--------------------------------------------------------------|
| <kbd>Ctrl</kbd> + <kbd>n</kbd>  | affiche un Nouveau script/texte/document vide                |
| <kbd>Ctrl</kbd> + <kbd>o</kbd>  | affiche la boite de dialogue pour Ouvrir un fichier existant |
| <kbd>Ctrl</kbd> + <kbd>s</kbd>  | Sauvegarde (enregistre) le fichier en cours d'édition        |
| <kbd>Ctrl</kbd> + <kbd>z</kbd>  | Annule (défait) la dernière édition réalisée sur le script ouvert |
| <kbd>Ctrl</kbd> + <kbd>c</kbd>  | Copie le texte surligné dans le presse papier                |
| <kbd>Ctrl</kbd> + <kbd>x</kbd>  | coupe le texte surligné dans le presse papier                |
| <kbd>Ctrl</kbd> + <kbd>v</kbd>  | colle le texte du presse papier à l'emplacement du curseur   |
| <kbd>Ctrl</kbd> + <kbd>f</kbd>  | recherche (Find) le texte surligné dans la fenêtre ouverte   |
| <kbd>Ctrl</kbd> + <kbd>h</kbd>  | remplace le texte surligné par le texte indiqué              |
| <kbd>Ctrl</kbd> + <kbd>F5</kbd> | enregistre puis exécute le script en cours d'édition (pour les IDE) |


L'appel à `Rechercher` ouvre une boîte ou une barre de dialogue pour connaître
l'expression à chercher dans le document (ou autre). La boîte de dialogue
permet ensuite de sauter d'une occurrence du texte à chercher à la suivante.
Cette recherche peut être ou non sensible à la casse, c'est-à-dire tenir compte
ou pas des différences majuscule/minuscule entre le texte cherché et les
occurrences du texte présent dans le document.
La fonctionnalité `Remplacer` demande un autre texte et remplace les occurrences
du texte recherché par le texte indiqué dans `Remplacer par`. Selon les
logiciels, plusieurs modes de remplacement peuvent être proposés tels que :

- `Rechercher` : surligne la prochaine occurrence du texte cherché,
- `Remplacer` : remplace l'occurrence surlignée du texte,
- `Remplacer et rechercher` : remplace l'occurrence surlignée puis cherche la prochaine occurrence,
- `Remplacer tout` : remplace systématiquement toutes les occurrences du texte cherché.

Pour les programmeur·es, cela permet de changer le nom d'une variable, ou
de localiser toutes les lignes de code qui utilisent une fonction. 

**{-à faire-}** 

- Dans le script donné précédemment, remplacer toutes les occurrences de v par variable
- que se passe-t'il si vous faites le remplacement une deuxième fois ?

## → Fonctionnalités proposées par Thonny

### Gérer des paquets/librairies

Python comprend des librairies (appelées aussi paquets) qui contiennent
des variables et fonctions déjà définies. Il est possible de les 
utiliser à l'aide des instructions d'import (voir UE Prog). Certaines
ne sont pas installées en même temps que Python et doivent être téléchargés
pour les utiliser. Thonny permet de le faire via son interface et le menu
`Outils>Gérer les paquets`. 

**{-à faire-}**  Installer le paquet `Turtle`, une célèbre librairie 
de dessin dont il existe une version Python. 

**{+remarque+}** De la même façon, Thonny propose de `Gérer les plugins`
qui sont souvent des extensions de Thonny lui-même. 
Si vous voulez utiliser le paquet L1Test avec Thonny sur votre ordinateur,
vous pouvez l'installer via la boîte de dialogue dédiée aux plugins.


### Interrompre un script en cours d'exécution

Il arrive que le·la programmeur·e souhaite interrompre l'exécution de son
code soit parce qu'il est long et qu'iel n'a pas la patience d'attendre 
la fin de l'exécution, soit parce que le code est entré dans une boucle infinie
(vous comprendrez plus tard dans l'UE Programmation). 
Dans Thonny, pour interrompre l'exécution d'un code, il faut appuyer sur le bouton
en forme de panneau STOP, ou utiliser le raccourci clavier <kbd>Ctrl</kbd> + <kbd>F2</kbd>.

**{-à faire-}**  

- Dans un script vide, copier-coller le script ci-dessous,
- lancer son exécution,
- observer qu'une fenêtre de dessin s'ouvre puisque le script utilise le paquet `Turtle`,
- attendre un peu pour prendre conscience que le curseur continue à dessiner indéfiniment,
- arrêter l'exécution du script.


```{python}
# Équipe pédagogique de ODI
# Ce script réalise un calcul mathématique très compliqué ;)
# 09/2023

from turtle import *

# Dessine des étoiles de plus en plus grandes 
pencolor("red")
i = 0
while True:
    forward(i * 10)
    right(144)
    i += 1

# Dessine un soleil
speed(0)
color('red', 'yellow')
begin_fill()
forward(200)
left(170)
while abs(pos()) >= 1:
    forward(200)
    left(170)
end_fill()
done()
```

### Commenter/décommenter du code

Pour différentes raisons, il peut être utile de commenter des
lignes de code pour qu'elles ne soient pas exécutées avec le reste
du script, pour les décommenter plus tard.
Dans le menu `Édition` de Thonny, des commandes permettent de commenter
et décommenter des lignes de codes. Ces commandes agissent sur l'ensemble 
des lignes surlignées au moment où elles sont lancées.

**{-à faire-}**  

- Observer le script précédent et remarquer qu'il contient deux parties,
identfiées chacune par une ligne de commentaire qui décrit le code qui suit.
- Les deux parties du code ont-elles été exécutées avant que vous n'arrêtiez
l'exécution ?
- Comment faire pour exécuter le code de la deuxième partie ?


### Auto-complétion

Thonny connaît les mots-clés du langage Python et aussi les noms des
variables, fonctions *etc.* du script qui a été exécuté. Lorsque l'on
commence à taper les premières lettres d'un mot que connaît Thonny,
l'utilisation du raccourci clavier <kbd>Ctrl</kbd> + <kbd>`Espace`</kbd>
permet de faire apparaître la liste des mots quio commencent pas les
lettres déjà tapées.
