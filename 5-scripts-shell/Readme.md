> # Scripts shell
> [Outils pour le développement informatique](../Readme.md) \
> 1re année de licence maths-info, Univ. Lille \
> octobre 2023 \
> [![](https://mirrors.creativecommons.org/presskit/icons/cc.png){width=20}![](https://mirrors.creativecommons.org/presskit/icons/by.png){width=20}](https://creativecommons.org/licenses/by/4.0/deed.fr)

# 1. Scripts shell

De [Wikipédia](https://fr.wikipedia.org/wiki/Script_shell) :
> Un script shell est un programme informatique développé pour fonctionner dans un shell Unix, un interpréteur de commandes. 

De [Ubuntu-fr](https://doc.ubuntu-fr.org/tutoriel/script_shell) :
> Un script shell permet d'automatiser une série d'opérations. Il se présente sous la forme d'un fichier contenant une ou plusieurs commandes qui seront exécutées de manière séquentielle.

# 2. Mon premier script

**{-à faire-}** Créer un dossier de travail et s'y placer au terminal.

**{-à faire-}** Créer un fichier de texte appelé `mon_premier_script.sh` avec le contenu suivant :
```
#!/bin/bash

NFICHIERS=7

echo Début d\'exécution du script.
echo
echo Voici les "$NFICHIERS" premiers fichiers de mon dossier maison :
find ~/ -maxdepth 1 -type f -ls | head -n "$NFICHIERS"
echo
echo Fin d\'exécution du script.
```

**{-à faire-}** Exécuter la commande `bash mon_premier_script.sh`

**{-à faire-}** Exécuter la commande `ls -l mon_premier_script.sh`. Ensuite, exécuter la commande `chmod +x mon_premier_script.sh`. Pour finir, exécuter à nouveau `ls -l mon_premier_script.sh` : quelle différence notez-vous entre la sortie de cette commande avant et après l'exécution de `chmod` ? 

**{-à faire-}** Exécuter la commande `./mon_premier_script.sh`. 

**{+remarque+}** La commande `./mon_premier_script.sh` ne réussit pas si le fichier `mon_premier_script` n'existe pas dans le dossier courant ou si le fichier n'a pas les _droits d'exécution_. Consulter la page du manuel de `chmod` pour plus d'informations.
 
**{-à faire-}** Interpréter le contenu de `mon_premier_script.sh` par rapport à la sortie obtenue. Décrire l'effet de chaque commande contenue dans le script.

# 3. Écrire un script shell

**{-à faire-}** Écrire un script shell qui à partir de ce [lien](https://gitlab.univ-lille.fr/ls1-odi/portail/-/raw/master/5-outils-fichiers-texte/dictionnaire-fr.txt.gz) produit un fichier nommé `table_de_10_mots.md` avec le contenu suivant :
```
| Ligne | Mot |
| --- | --- |
|     1|a|
|     2|à|
|     3|AAAI|
|     4|abaissa|
|     5|abaissable|
|     6|abaissables|
|     7|abaissai|
|     8|abaissaient|
|     9|abaissais|
|    10|abaissait|
```
Suggestion : dans votre historique de commandes shell `history` vous pouvez normalement récupérer toutes les commandes nécessaires, que vous avez déjà exécuté pendant [ce travail pratique](../5-outils-fichiers-texte/Readme.md).

# 4. Interpréter un script shell complexe

**{-à faire-}** Télécharger et exécuter le [script mystère](./script_mystere.sh).

**{-à faire-}** Deviner le fonctionnement du script mystère en exécutant une par une les commandes à son intérieur et en consultant les pages de manuel de `cut`, `null`, `sed`, `xargs`, ainsi que ce [tutoriel sed](https://debian-facile.org/doc:systeme:sed).

**{-à faire-}** Modifier le script pour obtenir seulement les lignes concernant les parcours informatiques.

**{-à faire-}** Modifier le script pour inclure le salaire brut annuel estimé.

**{-à faire-}** Écrire un nouveau script qui produit le même type de sortie mais triée en ordre décroissant de salaire mensuel. Suggestion : les options `-r` et `-n` de la commande `sort` pourraient vous aider.


# 5. Pour aller plus loin

Voir [Wikipédia - shell scripts](https://en.wikipedia.org/wiki/Shell_script), [Wikipédia - Bash](https://fr.wikipedia.org/wiki/Bourne-Again_shell), [Ubuntu-fr](https://doc.ubuntu-fr.org/tutoriel/script_shell).

