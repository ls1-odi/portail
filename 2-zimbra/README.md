> # Découverte de Zimbra, client de messagerie électronique et de gestion de calendriers
> [Outils pour le développement informatique](../Readme.md) \
> 1re année de licence maths-info, Univ. Lille \
> septembre 2023 \
> [![](https://mirrors.creativecommons.org/presskit/icons/cc.png){width=20}![](https://mirrors.creativecommons.org/presskit/icons/by.png){width=20}](https://creativecommons.org/licenses/by/4.0/deed.fr) 

# Objectifs

Dans le cadre de vos études et de votre vie professionnelle,
vous serez amenés à communiquer en utilisant vraisemblablement une
messagerie électronique, et à organiser votre emploi du temps à l'aide de
calendriers.

Dans ce TDM, nous vous proposons de découvrir, ou redécouvrir, les points
suivants :
 - les courriels
 - les agendas.

Les manipulations se feront à l'aide de la suite *Zimbra*, qui est celle utilisée par l'Université de Lille. 
Cependant, ces manipulations seront transférables sur d'autres suites.

Vous répondrez aux questions dans un document texte appelé `odi-tp2-nom1-nom2.txt`.
En fin de séance, vous enverrez un courriel à votre enseignant d'ODI
contenant vos travaux.

Si vous souhaitez consulter vos courriels ou votre agenda directement sur votre
téléphone, il est possible de suivre [ce tutoriel pour téléphones sous
Android](https://infotuto.univ-lille.fr/fiche/messagerie-android) ou [celui
pour téléphones sous iOS](https://infotuto.univ-lille.fr/fiche/messagerie-ios).
Dans ce cas, il vous est déconseillé de synchroniser les contacts.

# 1. Premiers pas avec *Zimbra*

*Zimbra* est une suite de logiciels de collaboration.

**{-à faire-}** Lancez un navigateur web et saisissez l'URL
`https://zimbra.univ-lille.fr`, puis connectez-vous à l'aide de votre
identifiant universitaire.

**{+remarque+}** Lors de la saisie de votre identifiant, il est inutile
de taper le domaine `@univ-lille.fr`.

**{-à faire-}** Si vous partagez un poste informatique avec votre binôme,
lancez une fenêtre de navigation privée
(<kbd>Ctrl</kbd>+<kbd>Shift</kbd>+<kbd>P</kbd> avec Firefox) et laissez votre
camarade se connecter à *Zimbra* dans cette fenêtre.

**{+remarque+}** Cette [fenêtre de navigation privée](https://fr.wikipedia.org/wiki/Navigation_priv%C3%A9e)
permettra de vous connecter à deux sessions *Zimbra* différentes, et surtout
de supprimer les [témoins de connexion](https://fr.wikipedia.org/wiki/Cookie_(informatique))
(*cookies*) à la fermeture de cette fenêtre.

Vous devriez voir une page web affichant les courriers électroniques
(*courriels* ou *e-mails*) reçus.
En effet, *Zimbra* contient un *client de messagerie*, vous permettant
de consulter, trier et rédiger des courriels.

**{+remarque+}** Un [client de messagerie](https://fr.wikipedia.org/wiki/Client_de_messagerie)
permet d'envoyer et de recevoir des courriels en communiquant avec un ou
plusieurs [serveur de messagerie](https://fr.wikipedia.org/wiki/Serveur_de_messagerie_%C3%A9lectronique).
Dans le cas de *Zimbra*, ce client de messagerie est une application web
à laquelle vous accédez en vous connectant à un serveur web.

Si vous regardez en haut à droite de cette page, vous devriez voir plusieurs
onglets. Vous êtes normalement sur celui appelé `Mail`.

![Les onglets de *Zimbra*](./img/zimbra_onglets.png)

Cliquez sur le deuxième onglet, `Contacts`. Vous devriez voir une page
affichant une liste de noms ou d'adresses électroniques. Il s'agit d'un
annuaire permettant de gérer les contacts.
L'Université de Lille propose un annuaire contenant les adresses électroniques
des étudiants et personnels.

**{-à faire-}** En utilisant cet annuaire, cherchez votre enseignant. Quelles
sont les informations disponibles ?

**{-à faire-}** Trouvez votre binôme.

Cliquez sur le troisième onglet, `Calendrier`. Vous devriez voir un calendrier
hebdomadaire. Il s'agit d'un agenda partagé permettant de consulter ou
fixer des évènements (cours ou réunions par exemple).

**{-à faire-}** En bas à gauche, un petit calendrier vous permet d'accéder
directement à une date donnée. À quel jour de la semaine aura lieu votre
prochain anniversaire ?

# 2. Client de messagerie

Nous allons commencer par configurer *Zimbra* pour la rédaction des messages.

**{-à faire-}** Allez dans l'onglet `Préférences`. Dans le panneau de gauche,
choisissez `Mail`. Modifiez si besoin les options suivantes :

 - afficher le mail en *format texte* (et pas HTML) ;
 - créer en *format texte* ;
 - enregistrer une copie dans le dossier Envoyés *activé* ;
 - réponse par mail : *inclure le mail original*, utiliser un préfixe *activé*
   et inclure les entêtes *activé* ;
 - faire suivre un mail : *inclure le mail original*, utiliser un préfixe *activé*
   et inclure les entêtes *activé* ;
 - préfixe des mails inclus `>`.

Ces options ne sont demandées que pour ce TP. Une fois celui-ci terminé, vous
pourrez choisir les options vous convenant.

Retournons sur l'onglet `Mail` pour nous intéresser au client de messagerie
**sans cliquer** sur le dit onglet.

**{-à faire-}** Appuyez successivement sur les touches <kbd>G</kbd> et
<kbd>M</kbd> (pour « **G**érer les **M**essages » ou *« **G**o to e-**M**ails »*
dans la langue de Justin Bieber).

Le client de messagerie affiche par défaut trois panneaux :

 - celui de gauche contient les dossiers et les libellés,
 - celui du milieu contient une liste des courriels contenus dans le
   dossier ou étiquetés par le libellé choisi,
 - celui de droite contient l'entête et le corps du courriel sélectionné.

## → Premier courriel et contenu brut

Pour avoir une vision plus claire de la messagerie électronique, nous allons
disséquer un courriel.

**{-à faire-}** Utilisez le bouton droit de la souris pour télécharger 
[ce fichier](exos/piece_jointe.png), en choisissant la bonne entrée du
menu contextuel qui apparaît. 
En utilisant la commande `ls -lh `_`CHEMIN_VERS_PIECE_JOINTE_PNG`_ dans le terminal, indiquez la taille en Kio de ce fichier.

**{+remarque+}** Lorsqu'on utilise l'option `-h` de la commande `ls`, les poids
des fichiers ne sont pas affichés avec le système international et un préfixe
décimal, mais avec un préfixe décimal. Cependant, pour des raisons historiques,
l'unité est affichée sous une forme simplifiée. Par exemple un fichier
pesant 10 kibioctets ne sera pas affiché `10 Kio` mais `10K` (ce fichier n'est
pas conservé à une température de 10 Kelvins !).

**{-à faire-}** Dans Zimbra, créez un nouveau message (<kbd>N</kbd>+<kbd>M</kbd>).

 - Dans le champ destinataire (`À`), renseignez l'adresse de votre binôme.
 - Dans le champ sujet, renseignez *« test »*.
 - Dans le corps du message, saisissez le texte suivant :
 
   ```
   Bonjour,
   Voici un message contenant des caractères accentués.
   Merci de votre attention.
   ```
 - Ajoutez en pièce jointe le fichier `piece_jointe.png`.
 - Envoyez le message.

**{-à faire-}** Cliquez avec le bouton droit sur le message envoyé par votre
binôme et choisissez *Montrer l'original*.

Dans cette fenêtre nouvellement ouverte, vous pouvez admirer les entrailles
du courriel : il s'agit d'un vulgaire fichier texte. Même la pièce jointe est
codée en texte !

**{-à faire-}** Sélectionnez tout le texte de ce courriel
(rappel : <kbd>Ctrl</kbd><kbd>A</kbd>), copiez-le (rappel :
<kbd>Ctrl</kbd><kbd>C</kbd>) et collez-le (rappel : <kbd>Ctrl</kbd><kbd>V</kbd>)
dans un éditeur de texte. Sauvegardez ce contenu dans un fichier nommé
`contenu_courriel.txt`.

**{-à faire-}** Quel est le poids (en Kio) de ce fichier ?

Observez maintenant la version texte du courriel. Celle-ci se présente
comme une séquence d'informations sous la forme _`Nom du champ: valeur du champ`_
dans un entête, puis le contenu du message.

**{-à faire-}** Quelles sont les valeurs des champs `From`, `To` et `Subject` ?
Quelle surprise !

Les autres champs sont les suivants :

 - `Received` permet de suivre le trajet du courriel. Si vous aviez utilisé
   un client local, on y aurait découvert l'adresse IP de la machine sur laquelle
   est exécuté ce client.
 - `Date` est une date ajoutée à la réception, purement indicative.
 - `Message-ID` est un identifiant créé à l'envoi du courriel par le serveur d'envoi
   de messagerie et utilisé par le serveur de réception.
 - `Content-Type` permet de déterminer le format du corps : dans ce message,
   on a un contenu en plusieurs parties, séparées par la chaîne renseignée
   juste après `boundary=`.
 - `X-Mailer` et `X-Originating-IP` permettent d'identifier le client.
   Remarquez que Zimbra ajoute en plus des informations sur votre navigateur
   web...

Le corps du message est ici en plusieurs parties, ayant chacune un entête
permettant au client de savoir comment l'afficher.

**{-à faire-}** Où sont passés vos caractères accentués ?

Dans l'entête de la première partie, on observe `charset=utf-8`.

**{-à faire-}** Dans l'article Wikipedia sur le [codage UTF8](https://fr.wikipedia.org/wiki/UTF-8), cherchez la représentation hexadécimale du codage du caractère `é`.
Proposez la version texte du corps d'un courriel ayant le message suivant :

```
À l'école PITA, j'ai perdu 8 211€.
```

Regardons maintenant la seconde partie du corps. On peut y voir le
format de fichier de la pièce jointe et son nom. De plus, on observe
`Content-Transfer-Encoding: base64` qui indique que la pièce jointe
a été codée en base 64.

**{-à faire-}** Dans le terminal, saisissez la commande `base64 `_`CHEMIN_VERS_PIECE_JOINTE_PNG`_. Qu'observez-vous ?

## → Champs d'un courriel, réponse et suivi

Lorsque vous rédigez un courriel, vous devez remplir plusieurs champs.

- Le champ `À` (`To:`) indique les destinataires directs. Il s'agit
  des personnes à qui le message est directement adressé. On peut y saisir directement
  l'adresse électronique ou alors un couple (pseudonyme, adresse électronique),
  par exemple `Responsable ODIEUX 1 <cristian.versari@univ-lille.fr>`. Attention,
  les personnes recevant ce courriel verront le pseudonyme saisi !
  En cas de plusieurs destinataires, il faut les séparer par des virgules.

  Lorsque vous composez un message sur Zimbra, et que vous commencez à saisir
  un nom ou une adresse, le client vous propose de compléter à l'aide
  de l'annuaire des contacts. Dans la version configurée par l'Université de Lille,
  cet annuaire contient les adresses de tous les personnels et étudiants.
  Le client Zimbra propose aussi les adresses lues dans les anciens messages
  envoyés ou reçus.

- Le champ `Cc` (`Cc:`) pour **c**opie **c**arbone indique les personnes
  pouvant être concernées par ce message, mais n'étant pas les destinataires.
  Par exemple, si vous souhaitez vous excuser d'une absence à une séance de TP,
  vous pouvez envoyer un courriel à votre chargé de TD en destinataire et à
  votre secrétaire pédagogique en copie carbone.

  Ce champ se remplit comme le champ `À`.

- Le champ `Cci` (`Bcc:`) pour copie carbone **i**nvisible indique
  les personnes pouvant être concernées par ce message, mais qui ne seront
  pas connues des autres destinataires du message.

  Par exemple, vous pourriez envoyer un courriel sans destinataire direct (`À`)
  mais avec plusieurs destinataires en copie carbone invisible.
  Dans ce cas, aucun des destinataires ne saura à qui d'autre vous avez
  envoyé ce message.

  Pour accéder au champ `Cci` dans le rédacteur de messages de Zimbra, vous
  pouvez cliquer sur `Options->Afficher Cci`.

- Le champ `Sujet` (`Subject:`) contient l'objet du message. Essayez donc
  rester concis tout en permettant aux destinataires de saisir l'enjeu
  du courriel.

  En aucun cas, vous ne devez laisser ce champ vide ! Le client Zimbra
  vous avertira en cas d'oubli.

Vous pouvez ensuite rédiger votre courriel.

## → Signature automatique

La signature est importante pour l'identification de l'expéditeur par les
destinataires. Plutôt que de la taper lors de chaque rédaction, il est possible
de l'insérer automatiquement avec le client Zimbra.

**{-à faire-}**
Dans l'onglet des *préférences* (<kbd>G</kbd><kbd>P</kbd>), sélectionnez
*Signatures* puis *Nouvelle signature*. Donnez lui un nom, vérifiez que
le format est bien *texte simple* et saisissez :

```
--
Prénom NOM
Étudiant (ou étudiante) en Licence MI
numéro de groupe
NIP
```

en remplaçant évidemment les informations nécessaires. Votre NIP est votre
numéro étudiant indiqué sur votre carte d'étudiant.

La ligne `-- ` (tiret tiret espace) fait office de séparateur et peut être
utilisée par certains clients pour détecter la signature. Faites donc
bien attention de la saisir correctement, n'oubliez pas l'espace finale.

**{+remarque+}** Cette signature ne doit pas être confondue avec la *signature numérique* qui
est une méthode cryptographique afin de d'assurer les destinataires de
l'authenticité (le message provient bien de l'expéditeur) et de l'intégrité (le
message n'a pas été modifié après son envoi) du courriel.

## → Réponses et suivis

Lorsqu'on reçoit un courrier électronique, il est possible d'y *répondre*,
de *répondre à tous* ou de le *faire suivre*.

Vous allez essayer d'inférer expérimentalement le comportement sur les champs
de ces trois actions.
L'exercice suivant se fait par groupe de deux binômes d'étudiants, qui devront
chacun se choisir un rôle parmi **A**lastor et **B**litzo dans le premier binôme,
et **C**harlie et **D**ia dans le second.

**{-à faire-}** L'étudiant A doit rédiger un courriel ayant pour destinataire
direct B, pour destinataire en copie carbone C et pour destinataire en copie
carbone invisible D.

**{-à faire-}** Les étudiants B et C doivent successivement répondre, répondre à
tous et faire suivre à D le message de A, en ajoutant dans le corps du courriel
respectivement `Réponse`, `Réponse à tous` et `Faire suivre`. Vérifiez que
la signature automatique a bien été automatiquement ajoutée.

**{-à faire-}** Déduisez-en les règles de modification des champs d'un courriel
respectivement auquel on répond, auquel on répond à tous ou que l'on fait
suivre.

## → Recherche

Il est possible de chercher un message dans la boîte de réception.

Dans Zimbra, on recherche des mots.
Un mot est une suite de lettres ou chiffres consécutifs, précédée et suivie
par des espaces, des symboles de ponctuation ou des tirets.
Ainsi, la recherche simple sous Zimbra du mot `pierre` renverra les messages
contenant le sujet `une pierre de feu` ou venant de l'expéditeur
`pierre.allegraud@univ-lille.fr` ou dont le contenu cite `pierre-feuille-ciseaux`,
mais pas `pierres magiques` ou `pierrette.bres@pmu.fr`.

Il est cependant possible d'utiliser le caractère joker `*` de la façon suivante :

 - `*mot` cherche tous les mots se terminant par `mot` (`mot` est donc *suffixe*
 des mots recherchés).
 - `mot*` cherche tous les mots commençant par `mot` (`mot` est donc *préfixe*
 des mots recherchés).
 - `*mot*` cherche tous les mots contenant `mot` (`mot` est donc *facteur* des
 mots recherchés).

Par exemple `math*` permet de trouver `math`, `maths`, `mathématiques` et `matheux`.

Les recherches dans Zimbra ignorent la casse (majuscules ou minuscules) et
l'accentuation des lettres. Par exemple, la recherche `pres` pourra reconnaître
les messages contant `pres`, `Pres`, `près`, `prés` ou `PrÉS`.

**{-à faire-}** Téléchargez le fichier [`liste.ods`](exos/liste.ods), et envoyez-le
en pièce jointe à votre binôme.

**{-à faire-}** Dans la zone de recherche, saisissez `Calbuth`. Il est vraisemblable
que vous n'ayez envoyé (ou reçu) aucun message à un certain Raymond Calbuth, ni
que vous en ayez parlé dans les sujets ou contenus de message. Cependant, la
recherche a été *fructueuse*.

**{-à faire-}** Demandez à *montrer l'original* du message trouvé, et cherchez
dans cette nouvelle fenêtre (<kbd>Ctrl</kbd><kbd>F</kbd>) le texte
`Calbuth`. L'avez-vous trouvé ?

Peut-être `Calbuth` se trouve dans la pièce jointe ? Pour vérifier cette hypothèse,
nous allons vérifier si le fichier `liste.ods` contient le texte recherché en
utilisant la commande `grep`.

**{-à faire-}** Dans le terminal, tapez `grep Calbuth `_`CHEMIN_VERS_LISTE_ODS`_.
L'avez-vous trouvé ?

**{-à faire-}** Ouvrez la pièce jointe `liste.ods`, et cherchez `Calbuth`.
Qu'en concluez-vous ?

**{+remarque+}** Ici, la recherche ne s'est pas effectuée dans le navigateur, mais
sur le serveur de l'Université. Ce qui signifie que le fournisseur de service est
capable de lire et comprendre vos pièces jointes. Si le rédacteur de ce sujet était
de mauvaise foi, il pourrait dire qu'une entreprise américaine spécialisée
dans la recherche web proposant un service de messagerie électronique et
ayant abandonné le moto *Don't be evil* pourrait en profiter.

Il est fort probable que dans quelques mois, votre boite de réception soit
tellement remplie que des simples recherches renvoient trop d'informations.
Dans ce cas il est possible d'utiliser une recherche avancée.

Par exemple, pour rechercher tous les courriels envoyés par
Éric Wegrzynowski avec dans le sujet « Programmation », on peut taper la
requête suivante : `from:eric subject:programmation`. Remarquez que l'on
n'est pas obligé de taper le nom de famille (ouf) car l'adresse contient
le prénom séparé par un point `.` du nom de famille.

Voici les mots-clés utilisés pour les recherche :

 - `from:` pour le champs `De` (`From:`) ;
 - `to:` pour le champs `À` (`To:`) ;
 - `cc:` pour le champs `CC` ;
 - `subject:` pour le champs `Sujet` (`Subject:`) ;
 - `after:` suivi d'une date au format `j/m/a` pour les messages postérieurs à la
    date indiquée (Zimbra utilisera alors le champs `Date:`) ;
 - `before:` pour les messages antérieurs à la date indiquée ;
 - `tag:` suivi d'un libellé pour les messages étiquetés par ce libellé ;
 - `has:attachment` pour les messages avec pièce jointe ;
 - `attachment:` suivi d'un type de pièce jointe pour les messages
   avec au moins une pièce jointe du type indiqué.

Il est possible de combiner les recherche avec les opérateurs `AND`, `OR`, `NOT`
et des parenthèses. Par exemple la recherche `(from:a OR from:b) AND NOT to:c`
indiquera les messages qui ont été envoyés par a ou par b, et qui ne sont pas
à destination directe de c.

**{-à faire-}** Indiquez quatre requêtes pour obtenir les messages vérifiant
respectivement les quatre critères suivants :

 - envoyés par vous à votre binôme ;
 - envoyés par votre directrice des études, mais pas à vous ;
 - dont le sujet contient `programmation` ou `codage` et destinés à vous ;
 - reçus en septembre 2022 ou septembre 2023.


## → Dossiers et libellés

**{-à faire-}** Quels dossiers sont affichés dans le panneau de gauche ?
Quels libellés ?

Parmi ces dossiers, certains sont particuliers :

 - le dossier *Réception* contient les messages reçus qui n'ont ni été détectés
   comme indésirables, ni été filtrés (nous verrons comment utiliser les
   filtres plus loin) ;
 - le dossier *Envoyé* contient une copie des messages envoyés par l'utilisateur ;
 - le dossier *Brouillons* contient les messages en cours de rédaction, mais
   pas encore envoyés ;
 - le dossier *SPAM* contient les messages qui ont été marqués comme indésirables
   par un algorithme.

**{-à faire-}** Créez un dossier nommé *Moodle* qui servira à contenir les attestations de rendus
de TP envoyées par Moodle.

**{-à faire-}** Une fois ce dossier créé, partagez-le en lecture seule avec votre binôme de TP.

**{+remarque+}** La manipulation précédente permettra à votre binôme de quand même recevoir les
attestations de rendu même lorsque ce sera votre tour de déposer des fichiers
sur Moodle.

Les libellés sont des étiquettes permettant d’organiser plus facilement vos courriels.
Alors qu'un courriel ne peut appartenir qu'à un et un seul dossier, il peut
avoir zéro, un ou plusieurs libellés différents.

Nous proposons d'utiliser les libellés suivants :
 - *important* pour ... les messages importants,
 - *très important* pour les messages ... très importants (bravo ! vous suivez !),
 - *liste de diffusion* pour les messages passant par une liste de diffusion.

**{-à faire-}** Créez les trois libellés décrits précédemment. Vous pouvez
choisir les couleurs qui vous semblent les plus pertinentes.

## → Filtres

Il est possible de filtrer les messages reçus, c'est-à-dire d'appliquer une 
ou plusieurs actions sur ceux-ci en fonction de leur contenu.
Les filtres proposés par Zimbra sont assez simples : ils sont sous la forme
`si <<liste de conditions>> alors <<séquence d'actions>>`.

Pour que l'action soit appliquées en fonction de la liste de conditions,
on a deux possibilités pour le filtre :

 - soit *toutes* les conditions doivent être vérifiées, on parle
   alors d'une *clause conjonctive* ;
 - soit *au moins une* condition doit être vérifiée, on parle alors de *clause
   disjonctive*.

Un filtre peut être actif, et dans ce cas il s'appliquera à chaque message
envoyé ou reçu, ou inactif, et il devra alors être lancé manuellement.

Les filtres se lancent successivement dans l'ordre choisi par l'utilisateur.
Lorsqu'un message vérifie la liste de conditions d'un filtre, les filtres
suivants sont eux aussi testés, sauf la case `Ne pas appliquer d'autres
filtres` est cochée.

Voici un exemple de traitement utilisant trois filtres :

- Si `De` contient `timoleon` alors libeller avec `ourson`.
- Si `De` contient `fantasio` ou `De` contient `prunelle` alors détruire.
- Si `À` commence par `gaston` alors libeller avec `important`. STOP.
- Si `De` se termine par `univ-lille.fr` alors rediriger vers `glagaffe@dupuis.fr`.

Ces filtres sont actifs et appliqués sur la boite de réception associée à
l'adresse `gaston.lagaffe.etu@univ-lille.fr`.

**{-à faire-}** Que se passe-t-il si Gaston reçoit un message envoyé par `timoleon@univ-lille.fr`
   à destination directe de `gaston.lagaffe.etu@univ-lille.fr` ?

**{-à faire-}** Que se passe-t-il si Gaston reçoit un message envoyé par `timoleon@univ-lille.fr`
   à destination directe de `jeanne.hazuki@univ-lille.fr` et
   avec `gaston.lagaffe.etu@univ-lille.fr` en copie carbone ?

**{-à faire-}** Que se passe-t-il si Gaston reçoit un message envoyé par
   `mademoiselle.jeanne@paprunelle.be` à destination directe de
   `gaston.lagaffe.etu@univ-lille.fr` ?

Voici les règles qu'on souhaite appliquer :

 - On veut déplacer dans le dossier `Moodle` les messages de notification
   de devoirs rendus sur Moodle (remarquez que leurs sujets commencent par `Ne pas
   répondre à ce courriel`).
 - On veut libeller comme `important`s les messages dont vous êtes le destinataire
   direct envoyés depuis une adresse en `univ-lille.fr` mais pas d'une
   adresse étudiante (`etu@univ-lille.fr`)...
 - ... ou les messages envoyés par votre directrice des études ou votre
   secrétaire pédagogique...
 - ... sauf ci ces derniers vous sont directement destinés et dans ce cas
   on veut les libeller en `très important`s.
 - Enfin, les message dont l'entête a un champ `List-Id` qui existe
   devront être libellés par `liste de diffusion.

**{-à faire-}** Proposez une suite de filtres permettant de répondre au problème précédent.
   Indiquez par `STOP` les filtres qui stoppent le traitement en cas de succès.
   *Indication : l'ordre des règles n'est pas forcément l'ordre des filtres,
   vous pouvez combiner plusieurs de ces règles en un unique filtre ou
   créer plusieurs filtres pour une seule règle.*

**{-à faire-}** Dans Zimbra, implantez les filtres précédents.


# 3. Agenda partagé

Zimbra possède aussi un gestionnaire d'agendas partagés.
Ces agendas électroniques peuvent être partagés entre plusieurs collègues,
et importés sur différentes machines (par exemple sur un ordinateur pour
les remplir et sur un téléphone portable pour les consulter).

Il est possible de consulter plusieurs agendas électroniques simultanément. 
Chaque agenda contient zéro, un ou plusieurs évènements qui peuvent être
éventuellement répétés.

Voici les propriétés des évènements :

 - Le sujet est l'objet de l'évènement, et c'est ce qui sera affiché. On
   préférera donc un sujet à la fois concis et compréhensible.
   Ce champ est obligatoire.
 - L'endroit est le lieu physique dans lequel se déroule l'évènement.
 - Le calendrier est l'agenda qui contient l'évènement.
 - L'heure de début et l'heure de fin sont suffisamment explicites... Il est
   cependant possible de choisir un évènement sur toute la journée (dans Zimbra,
   il faudra alors cliquer sur `Plus de détails`).
 - Répéter permet de créer une série d'instances de cet évènement. On doit alors
   choisir la fréquence de répétition.
 - Le rappel permet au client d'envoyer des notifications avant l'évènement.
 - L'affichage permet aux personnes avec qui vous avez partagé votre
   agenda de connaître vos disponibilités au cours de cet évènement.
 - Si un évènement est marqué privé, le sujet et l'endroit seront cachés
   aux personnes partageant l'agenda qui n'ont pas été autorisées à
   consulter les évènements privés lors du partage.

Lorsqu'un évènement est répété, il est possible de modifier une seule instance
ou toutes les instances de cette série.
De plus, il est possible de supprimer une seule instance, toute la série, ou
toutes les instances de la série postérieures à une date donnée.


**{-à faire-}** Créez un agenda appelé `Cours`.

**{-à faire-}** Dans cet agenda, créez des évènements pour chacun de vos cours (uniquement les CM). 
Indiquez en lieu les salles. N'hésitez pas à utiliser intelligemment
   les instances et les séries.


Un agenda peut être partagé avec une ou plusieurs personnes.
Dans Zimbra, il suffit de cliquer avec le bouton droit sur l'agenda et choisir
partage. L'agenda peut alors être partagé de trois façons :

 - soit à d'autres personnes ayant une adresse `@univ-lille.fr`, on pourra
   alors régler finement le partage et les permissions ;
 - soit en envoyant une URL (par exemple par courriel), et dans ce cas l'agenda
   sera en lecture seule et les invités ne seront pas autorisés à consulter les
   évènements privés ;
 - soit en téléchargeant le fichier `ics` associé au calendrier et envoyant
   ce fichier, et dans ce cas les modifications ultérieures de l'agenda
   ne pourront pas être vues par les personnes ayant reçu le fichier.

**{-à faire-}** Partagez l'agenda `Cours` en lecture seule avec votre binôme.

**{-à faire-}** Téléchargez le fichier `ics` associé à votre agenda `Cours` et ouvrez-le
   à l'aide d'un éditeur de texte. Que constatez-vous ?

# 3. Rendu

En fin de séance, un des deux membres du binôme rendra par courrier électronique
un compte rendu du travail effectué.

**{-à faire-}** Composez un nouveau message :

 - dont le destinataire direct est votre enseignant d'ODI ;
 - avec en copie carbone votre binôme ;
 - dont le sujet est `[ODI] Rendu TP2 MIXX Nom1 Nom2` (en remplaçant
   évidemment `MIXX` par votre groupe et `Nom1` et `Nom2` par les
   noms des étudiants des binômes ;
 - contenant en pièce jointe un fichier nommé `odi-tp2-nom1-nom2.ics`
   qui est l'agenda de vos cours magistraux créé et téléchargé dans
   la partie précédente ;
 - et dont le corps, correctement rédigé, est le contenu du fichier
   `odi-tp2-nom1-nom2.txt`.
