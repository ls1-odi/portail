> # Formules mathématiques Latex en Markdown
> [Outils pour le développement informatique](../Readme.md) \
> 1re année de licence maths-info, Univ. Lille \
> octobre 2023 \
> [![](https://mirrors.creativecommons.org/presskit/icons/cc.png){width=20}![](https://mirrors.creativecommons.org/presskit/icons/by.png){width=20}](https://creativecommons.org/licenses/by/4.0/deed.fr)


Certains dialects Markdown permettent l'écriture de formules mathématiques en style 
[LaTeX](https://fr.wikipedia.org/wiki/LaTeX). Par exemple, la formule suivante

<p align="center">
![Exemple de formule LaTeX](./img/formule.png){ width=160px }
</p>

peut être obtenue à partir du bloque de code LaTeX ci-dessous :

```latex
$$
x={\frac {-b\pm {\sqrt {b^{2}-4ac}}}{2a}}
$$
```

**{+remarque+}** Beaucoup de logiciels pour la mise en forme de documents
Markdown, comme par exemple certaines versions de Okular, ne supportent 
pas la mise en forme de formules mathématiques en style LaTeX.

**{+remarque+}** Pandoc permet la génération de code HTML 
à partir de documents Markdown contenants des formules mathématiques 
en style LaTeX, grâce à des options spécifiques :

```bash
pandoc -t html -s --mathjax document.md -o document.html
```

**{+remarque+}** Pandoc permet aussi la génération de documents PDF
à partir de documents Markdown avec formules mathématiques, en spécifiant
optionnellement le *moteur PDF* à utiliser pour la génération :

```bash
pandoc --pdf-engine=lualatex document.md -o document.pdf
```

**{+remarque+}** Voir `man pandoc` pour plus d'informations.

**{-à faire-}** Étudier [l'écriture de formule mathématiques en LaTeX](https://zestedesavoir.com/tutoriels/826/introduction-a-latex/1322_completer-vos-documents/5376_des-mathematiques/).

**{+remarque+}** Selon le dialecte Markdown utilisé, l'ensemble de balises et commandes
reconnues pour la mise en forme de formules LaTeX dans un document Markdown 
peut varier sensiblement.

**{-à faire-}** Écrire un document Markdown `formule.md` qui permet de générer via 
Pandoc [cette page](./formule/formule.pdf).

## Pour aller plus loin

LaTeX est un langage complexe pour la redaction de documents. Les fichiers LaTeX
ont généralement une extension `.tex`.

**{-à faire-}** Lire [ce tutoriel du langage LaTeX](https://math.univ-lyon1.fr/irem/IMG/pdf/LatexPourLeProfDeMaths.pdf),
en particulier le chapitre 2.

**{+remarque+}** La *compilation* d'un fichier LaTeX permet de générer un document
PDF mis en forme. Un des *moteurs* de compilation LaTeX disponibles dans les salles
de TP est `lualatex`. La commande suivante permet de *compiler* un fichier `.tex` :

```bash
lualatex votre_document_LaTeX.tex
```

**{-à faire-}** Rédiger un fichier `formule.tex` en format LaTeX qui 
permet d'obtenir [cette page](./formule/formule.pdf).

**{+remarque+}** Pandoc permet de générer du code LaTeX à partir d'un document
Markdown :

```bash
pandoc -s -t latex --pdf-engine lualatex votre_document_markdown.md -o votre_document_LaTeX.tex
```

**{-à faire-}** Exécuter la commande `man pandoc` pour découvrir la signification 
de chacune des options de la commande ci-dessus.

**{-à faire-}** Générer le code LaTeX correspondant au document `formule.md` à l'aide de 
la commande suivante :

```bash
pandoc -s -t latex --pdf-engine lualatex formule.md -o formule_via_pandoc.tex
```

**{-à faire-}** Comparer le contenu du fichier `formule.tex` rédigé précédemment et
du fichier `formule_via_pandoc.tex` que vous venez de générer. Quelles différences
pouvez-vous remarquer ?

**{-à faire-}** Compiler le fichier `formule_via_pandoc.tex`, puis vérifier que la
mise en forme du document PDF généré correspond à [l'original](./formule/formule.pdf).


<!-- eof --> 
