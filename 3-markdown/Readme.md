> # Produire des documents avec Markdown
> [Outils pour le développement informatique](../Readme.md) \
> 1re année de licence maths-info, Univ. Lille \
> octobre 2023 \
> [![](https://mirrors.creativecommons.org/presskit/icons/cc.png){width=20}![](https://mirrors.creativecommons.org/presskit/icons/by.png){width=20}](https://creativecommons.org/licenses/by/4.0/deed.fr)

# 1. Markdown

Un fichier au _format texte_ est une simple séquence de caractères. 
Ce texte peut être un texte "simple", du code Python, et bien d'autres choses. 

Un _document texte_ est quand à lui un texte enrichi de mise en forme, par exemple mettre en gras ou italique, mentionner des titres de sections, etc. 

Un document texte peut être produit à l'aide d'un logiciel dédié tel _LibreOffice Writer_ de la suite bureautique libre [LibreOffice](https://fr.wikipedia.org/wiki/LibreOffice), ou [Microsoft Word](https://fr.wikipedia.org/wiki/Microsoft_Word), logiciel [privateur](https://fr.wikipedia.org/wiki/Logiciel_propri%C3%A9taire) publié par Microsoft. 

Une alternative à l'utilisation d'une suite bureautique est de décrire la mise en forme du texte dans le fichier texte lui-même. 
On utilise alors une convention, ensemble de règles formelles qui définissent comment structurer, comment donner forme à quelque chose.

Vous connaissez sans doute le [langage HTML](https://fr.wikipedia.org/wiki/HTML) utilisé pour décrire des pages web.  
Un fichier HTML est un fichier au format texte dans lequel des [balises](https://fr.wikipedia.org/wiki/%C3%89l%C3%A9ment_HTML) précisent la mise en forme.  
Un navigateur web est ensuite utilisé pour visualiser ces pages. 

L'objet de ces travaux pratiques est de découvrir le langage _Markdown_, langage de balisage léger permettant de produire des documents texte.  
On écrira par exemple `ce *texte* est mis en évidence` pour signifier que le mot "texte" doit être mis en forme en italique, c'est-à-dire avec des caractères inclinés comme ceci : "ce *texte* est mis en forme". 

# 2. Mon premier fichier Markdown 

L'archive téléchargeable via la [page suivante](xampl1.zip) contient deux fichiers.

**{-à faire-}** Télécharger et décompresser cette archive. Quels fichiers contient-elle ? 

**{-à faire-}** Ouvrir le fichier d'extension `.md` avec votre éditeur de texte (Thonny par exemple). 

**{-à faire-}** Ouvrir ce même fichier `.md` avec le visualiseur de documents Okular. 

**{-à faire-}** Créer un fichier nommé `rapport-markdown.md` dans lequel rédiger le compte-rendu de ce TP.  
Donner la liste [**exhaustive**](https://fr.wiktionary.org/wiki/exhaustif) des [mises en page](https://fr.wikipedia.org/wiki/Mise_en_page) utilisée dans le fichier Markdown de l'archive. 

# 3. Comme un manuel Markdown

Markdown a été conçu par [John Gruber](https://fr.wikipedia.org/wiki/John_Gruber) avec l'aide d'[Aaron Swartz](https://fr.wikipedia.org/wiki/Aaron_Swartz). 

**{-à faire-}** Consulter les pages Wikipedia des créateurs de Markdown.  
Quel est leur âge respectif quand ils conçoivent Markdown ? 

Des variantes de Markdown ont ensuite été proposées et des outils de visualisation ou de traduction de Markdown vers d'autres formats (HTML, PDF, etc.) publiés. 

Il n'existe pas de version standard de Markdown. 

Les différences entre deux versions de Markdown sont minimes, on se contraint parfois à modifier son texte Markdown pour être conforme avec la variante ou l'implémentation choisie.  
On consultera pour cela la documentation associée... quand elle existe !  
À défaut, on procède par essais-erreurs. 

La variante utilisée par Okular est peu documentée. (On pourra consulter la page [pell.portland.or.us/~orc/Code/markdown/](https://www.pell.portland.or.us/~orc/Code/markdown/) ou se référer au code source de l'implémentation disponible à [github.com/Orc/discount](https://github.com/Orc/discount)).

On se basera donc sur un autre manuel pour découvrir l'ensemble des fonctionnalités de Markdown, et ajusterons en fonction de ce que permet effectivement Okular. 

Nous vous conseillons le livre de Bernard Pochet _Markdown & vous_ disponible librement sur [le site de l'Université de Liège (B)](https://e-publish.uliege.be/md). 

**{-à faire-}** Parcourir le [chapitre "3. Markdown"](https://e-publish.uliege.be/md/chapter/markdown/) de _Markdown & vous_. 

# 4. Écrire du Markdown

L'archive téléchargeable via la [page suivante](recette.zip) contient plusieurs fichiers. Dont un fichier HTML généré à partir d'un document Markdown. 

**{-à faire-}** Télécharger et décompresser cette archive. 

**{-à faire-}** Ouvrir le fichier HTML contenu dans l'archive dans un nouvel onglet de votre navigateur web.

**{-à faire-}** En vous basant sur les connaissances de Markdown déjà acquises,
rédiger un fichier `recette.md` qui reproduit le contenu de la page HTML et reflète sa mise en forme une fois affiché avec Okular.

# 5. Convertir du Markdown en d'autres formats

Les documents Markdown peuvent être utilisés pour la génération de documents en différents formats. Un des outils les plus
puissants pour la conversion entre formats différents est [Pandoc](https://pandoc.org/).

**{-à faire-}** Au terminal, se placer dans le dossier qui contient le fichier `recette.md` qui
vient d'être créé à l'aide de la commande `cd`.

**{-à faire-}** Convertir votre document Markdown `recette.md` en format HTML à l'aide de la commande suivante.

```bash
pandoc recette.md -o recette.html
```

**{-à faire-}** Vérifier la présence d'un nouveau fichier `recette.html` dans le dossier courant à l'aide de la commande
`ls`.

**{-à faire-}** Visualiser le fichier `recette.html` à l'aide de Firefox ou de votre navigateur web préféré :

```bash
firefox recette.html &
```

**{-à faire-}** Utiliser la commande suivante pour une conversion en format PDF, puis utiliser Okular pour ouvrir le fichier
PDF créé :

```bash
pandoc recette.md -o recette.pdf
```

**{-à faire-}** Utiliser la commande suivante pour une conversion en format ODT, puis utiliser LibreOffice Writer (commande `lowriter`)
pour ouvrir le fichier ODT créé :

```bash
pandoc recette.md -o recette.odt
```

**{-à faire-}** Afficher la liste des formats de sortie de `pandoc` grâce à la commande suivante :
```bash
pandoc --list-output-formats
```
Quels formats connaissez-vous parmi ceux disponibles via `pandoc` ?

# 6. Rendre votre rapport

**{-à faire-}** Envoyer un courriel au responsable de votre groupe d'ODI avec objet
`[ODI][GRP XY] TP Markdown` (où `XY` est toujours à remplacer par le numéro de votre groupe) contenant les documents `recette.md`, `rapport-markdown.md` en pièce jointe.  
Mettre votre binôme en copie. 


