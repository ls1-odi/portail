# Produire un diaporama avec Markdown
> [Outils pour le développement informatique](../Readme.md) \
> 1re année de licence maths-info, Univ. Lille \
> octobre 2023 \
> [![](https://mirrors.creativecommons.org/presskit/icons/cc.png){width=20}![](https://mirrors.creativecommons.org/presskit/icons/by.png){width=20}](https://creativecommons.org/licenses/by/4.0/deed.fr)


Le format [*beamer*](https://fr.wikipedia.org/wiki/Beamer) de Pandoc permet la génération 
de diaporamas à partir de documents Markdown.
Ces documents peuvent contenir un sous-ensemble des 
[notations mathématiques](https://en.wikibooks.org/wiki/LaTeX/Mathematics) 
en format [LaTeX](https://fr.wikipedia.org/wiki/LaTeX).

**{-à faire-}** Télécharger et décompresser [l'archive `diapo.zip`](https://gitlab.univ-lille.fr/ls1-odi/portail/-/raw/master/3-markdown/diapo.zip), se placer dans le dossier
correspondant et utiliser la commande suivante pour convertir le fichier `diapo.md` en format PDF :

```bash
pandoc -t beamer --slide-level 2 --pdf-engine=lualatex --table-of-contents  -s diapo.md -o diapo.pdf
```

**{-à faire-}** Utiliser la commande suivante pour ouvrir le diaporama qui vient d'être généré :

```bash
okular --presentation diapo.pdf
```

**{-à faire-}** En observant le diaporama `diapo.pdf`, étudier chaque ligne du fichier source 
`diapo.md`. Noter dans votre compte rendu toutes les commandes, annotations et balises qui 
déterminent la mise en forme du document, en incluant celles pour le choix du thème, 
la séparation des pages, la création de formules.

Le diaporama 
[`biblio_python.pdf`](https://gitlab.univ-lille.fr/ls1-odi/portail/-/raw/master/3-markdown/diaporama/biblio_python.pdf) 
a été généré à partir d'un document Markdown.

**{-à faire-}** En vous basant sur les connaissances de Markdown que vous avez acquises,
rédiger un fichier `biblio_python.md` qui reproduit autant que possible le contenu 
et la mise en forme de ce diaporama.

**{-à faire-}** Mettre à jour le fichier `rapport-markdown.md` avec vos réponses
puis l'envoyer au responsable de votre groupe d'ODI dans un courriel dont le sujet 
est `[ODI][GRP XY] TP Markdown - Pour aller plus loin` (où `XY` est toujours à 
remplacer par le numéro de votre groupe).  
Mettre votre binôme en copie. 

## Pour aller plus loin

**{-à faire-}** Lire la [documentation Pandoc pour la génération de diaporamas](https://pandoc.org/MANUAL.html#slide-shows).


<!-- eof --> 
