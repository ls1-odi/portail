> # Partage de fichiers avec Nextcloud
> [Outils pour le développement informatique](../Readme.md) \
> 1re année de licence maths-info, Univ. Lille \
> septembre 2023 \
> [![](https://mirrors.creativecommons.org/presskit/icons/cc.png){width=20}![](https://mirrors.creativecommons.org/presskit/icons/by.png){width=20}](https://creativecommons.org/licenses/by/4.0/deed.fr) 


# 1. Objectifs

Ce document vise à vous guider dans l'utilisation de Nextcloud avec votre compte
universitaire pour partager vos fichiers entre différents ordinateurs et/ou avec
d'autres utilisateurs et utilisatrices. 


# 2. Comment utiliser Nextcould ?

La fiche infotuto de l'université sur Nextcloud est complète, veuillez la lire 
pour pouvoir faire les exercices qui suivent :
[https://infotuto.univ-lille.fr/fiche/nextcloud](https://infotuto.univ-lille.fr/fiche/nextcloud)


## → Ranger son espace de travail

Pour avoir un espace de travail rangé, dans lequel on retrouve facilement
ses fichiers, il est nécessaire de créer des dossiers (ou répertoires). 
Nous vous conseillons de créer un répertoire par UE (ex : `maths`, `odi`, `prog`, etc.).

**{-à faire-}** S'identifier sur le Nextcloud de l'université. 

**{-à faire-}** Créer un dossier `odi` et un dossier `prog`, si ce n'est pas déjà fait. 
Dans le dossier `odi`, créer des sous-dossier appelés `tp1` et `tp2`.

## → Téléverser des fichiers

Normalement, vous avez sur votre poste de travail dans les répertoires de votre dossier personnel (dossier maison) des fichiers qui correspondent au
travail que vous avez fait lors des deux premières séances de ODI.

**{-à faire-}** Téléverser ces fichiers dans le dossier `odi/tp1` de votre Nextcloud.


## → Partager un fichier ou un répertoire

**{-à faire-}** Partager en lecture seule le répertoire `odi/tp1` avec le ou la binôme
avec qui vous aviez travaillé lors des deux premières séances ou avec votre voisin si
vous aviez travaillé seul. 


## → Récupérer un fichier ou un répertoire

**{-à faire-}** Sur un autre ordinateur (par exemple chez vous), vous connecter
au Nextcloud de l'université via un navigateur et télécharger les fichiers du 1er TP de ODI.


# 3. Travailler sur Nextcloud au terminal

L'arborescence de répertoires et fichiers distants disponibles sur votre espace Nextcloud 
peut être rendue accessible localement sur votre poste de travail grâce au protocole [WebDAV](https://fr.wikipedia.org/wiki/WebDAV).
Il est alors possible d'utiliser pour Nextcloud les outils de manipulation du système de fichiers vues lors du premier TP, y compris en ligne de comamnde dans le terminal.

## → L'adresse WebDAV de l'espace Nextcloud

Pour rendre disponible localement votre espace Nextcloud, il faut repérer son 
adresse WebDAV. Cette adresse se trouve dans le menu de Nextcloud, option 
"Settings" :

![Menu Nextcloud](./img/nextcloud-menu.png)

![Option Settings](./img/nextcloud-settings.png)

![Option Settings](./img/nextcloud-webdav-address.png)

**{-à faire-}** Accédez à votre espace Nextcloud et repérez son adresse WebDAV comme
montré ci-dessus.

**{+remarque+}** L'adresse WebDAV que vous venez de repérer commence normalement par `https://`
et se termine par un identificateur [alphanumérique](https://fr.wikipedia.org/wiki/Caract%C3%A8re_alphanum%C3%A9rique) du type `A12345`.

**{-à faire-}** Copiez l'adresse WebDAV de votre espace Nextcloud dans le presse-papiers.

## → Montage local de l'espace Nextcloud

La commande `gio mount` permet de rendre disponible localement dans le système de fichiers votre espace Nextcloud. 
On parle de _monter_, _point de montage_, vor par exemple la [page Wikipedia dédiée](https://fr.wikipedia.org/wiki/Point_de_montage) 
Pour utiliser cette comamnde, il faut lui passer en paramètre une adresse WebDAV :

```
gio mount davs://<adresse_WebDAV_de_votre_espace_nextcloud>
```

**{+remarque+}** L'adresse WebDAV de votre espace Nextcloud commence par `https://`
tandis que l'adresse à donner à la commande `gio mount` commence par `davs://`. Pour pouvoir monter
votre espace Nextcloud en exploitant la commmande `gio mount`, vous devez tout simplement 
 remplacer `https://` par `davs://`.

**{+remarque+}** La commande `gio mount` demande la saisie d'un nom d'utilisateur et mot de 
passe. Pour le montage de votre espace Nextcloud, cela correspond à votre adresse mail de 
l'université et mot de passe correspondant.

**{-à faire-}** Montez votre espace Nextcloud.

**{+remarque+}** En cas d'échec, la commande `gio mount` montre un message d'erreur.
Si cela se produit, vérifiez à nouveau l'exactitude de 
l'adresse WebDAV passée à `gio mount` et réessayez le montage.


## → Identification du répertoire de montage

En cas de succès, la commande `gio mount` rend disponible votre espace Nextcloud dans un répertoire en dehors de votre dossier maison. 
Pour identifier ce répertoire, vous devez connaître votre [_user identifier_](https://fr.wikipedia.org/wiki/User_identifier), abrégé _UID_.

**{-à faire-}** Utilisez la commande `id` pour connaître votre UID.

Le répertoire de montage de votre espace Nextcloud est un sous-répertoire de `/run/user`.

**{-à faire-}** Dans un terminal, placez-vous dans le répertoire `/run/user`.

**{-à faire-}** Affichez le contenu de ce répertoire à l'aide de la commande `ls`.

Parmi les dossiers et fichiers affichés par la commande précédente, vous devriez
voir un répertoire dont le nom correspond à votre UID.

**{-à faire-}** Placez-vous dans le sous-répertoire dont le nom correspond à votre UID.

**{-à faire-}** Affichez le contenu de ce répertoire.

Parmi les dossiers et fichiers affichés par la commande précédente, vous devriez
voir un répertoire nommé `gvfs`.

**{-à faire-}** Placez-vous dans le sous-répertoire `gvfs`.

**{-à faire-}** Affichez le contenu de ce répertoire.

Parmi les dossiers et fichiers affichés par la commande précédente, vous devriez voir maintenant un répertoire avec un nom très long qui contient l'adresse du serveur
Nextcloud de l'université `nextcloud.univ-lille.fr`. 
Votre espace Nextcloud est accessible dans ce répertoire.

**{-à faire-}** En exploitant la 
[complétion automatique](https://fr.wikipedia.org/wiki/Auto-compl%C3%A9tion#Interpr%C3%A9teurs_de_commandes)
du terminal, placez-vous dans le sous-répertoire qui correspond à votre espace
Nextcloud.

**{-à faire-}** Utilisez la commande `pwd` pour afficher le chemin du dossier courant. 
Vérifiez que ce chemin est bien de la forme

```
/run/user/<votre_UID>/gvfs/...nextcloud...etc...etc.../
```

**{-à faire-}** Utilisez la commande `ls -la` pour afficher le contenu courant de votre espace
Nextcloud. Vérifiez que cela correspond bien à ce que vous y voyez à travers le navigateur web.


## → Utilisation de l'espace Nextcloud au terminal

Les commandes au terminal vues pour la manipulation de dossiers et fichiers peuvent maintenant être utilisées pour manipuler le système de fichiers distant de votre espace Nextcloud.

**{-à faire-}** Utilisez convenablement les commandes `mkdir` et `cd` 
pour créer cet arbre de répertoires à l'intérieur de votre espace Nextcloud :

```
odi_tp_nextcloud_au_terminal/
├── arbre_de_dossiers
├── captures_d_ecran
├── dossier_courant
├── explication_des_commandes
└── historique_des_commandes
```

**{-à faire-}** Placez-vous dans le sous-répertoire `arbre_de_dossiers` en utilisant 
convenablement la commande `cd`.

**{-à faire-}** Exécutez la commande 

```
tree -puDo tree_output.txt ../../odi_tp_nextcloud_au_terminal/
```

**{-à faire-}** Listez maintenant le contenu du répertoire. Un nouveau fichier est
présent.

**{-à faire-}** Affichez le contenu du nouveau fichier avec la commande `cat` suivie du nom du fichier.

**{-à faire-}** Placez-vous dans le sous-répertoire `dossier_courant` toujours en utilisant 
convenablement la commande `cd`.

**{-à faire-}** Exécutez la commande 

```
pwd > dossier_courant.txt
```

**{-à faire-}** Listez le contenu du répertoire. Un nouveau fichier est présent.

**{-à faire-}** Affichez le contenu du nouveau fichier avec la commande `cat` suivie
par le nom du fichier.

**{-à faire-}** Placez-vous dans le sous-répertoire `historique_des_commandes`.
Exécutez la commande suivante et affichez à nouveau le contenu du nouveau fichier créé. 

```
history > historique.txt
```

**{-à faire-}** Faites une capture d'écran.

**{-à faire-}** Placez-vous dans le sous-répertoire `captures_d_ecran`. Utilisez
la commande suivante pour identifier le chemin et le nom du fichier qui contient
la capture d'écran que vous venez de faire, sans pour autant changer de dossier courant.

```
ls -l ~/Pictures/
```

**{+remarque+}** Le caractère `~` dans la commande précédente représente le dossier maison.

**{-à faire-}** Utilisez convenablement la commande `cp` pour copier le fichier
de la capture d'écran dans le dossier courant, comme suit :

```
cp ~/Pictures/<nom_du_fichier_qui_contient_la_capture_d_ecran> .
```

**{+remarque+}** Le caractère `.` dans la commande précédente représente
le dossier courant.

**{-à faire-}** Listez le contenu du dossier courant pour vérifier que le fichier
a été copié avec succès.

**{-à faire-}** Ouvrez l'éditeur Thonny. Créez avec Thonny un nouveau fichier nommé `explication_commandes.txt` 
à l'intérieur du sous-répertoire `explication_des_commandes`. Dans ce fichier texte, expliquez 
en détail le fonctionnement de toute commande utilisée pendant ce TP, en décrivant les paramètres passés 
à la commande, les effets produits, les éventuels fichiers créés et leurs contenu.

**{-à faire-}** Placez-vous dans le sous-répertoire `odi_tp_nextcloud_au_terminal`.
Exécutez la commande 

```
zip -r tp_nextcloud_au_terminal.zip *
```

**{-à faire-}** Affichez le contenu du dossier courant. Un nouveau fichier apparaît,
notamment une archive ZIP.

**{-à faire-}** Copiez ce fichier dans votre dossier maison à l'aide de la commande `cp` :

```
cp <nom_du_fichier_ZIP_que_vous_venez_de_creer> ~/
```

**{-à faire-}** Sans changer de dossier, vérifiez à l'aide de la commande `ls` que la copie
a été réalisée avec succès.

**{-à faire-}** Envoyez un message de courriel au responsable de votre groupe, ayant comme
objet `[ODI][GRP XY] TP Nextcloud au terminal` (ou `XY` est à remplacer par le numéro de
votre groupe) et en pièce jointe l'archive ZIP que vous venez de copier dans votre dossier maison. 
Mettez votre binôme en copie. 


## → Démontage de l'espace Nextcloud

La commande suivante permet de démonter votre espace Nextcloud :

```
gio mount -u davs://<adresse_WebDAV_de_votre_espace_nextcloud>
```

Pour l'exécuter avec succès, il faut d'abord sortir du répertoire de montage.

**{-à faire-}** Utilisez convenablement la commande `cd` pour sortir du répertoire de montage
et démontez votre espace Nextcloud.

**{-à faire-}** Utilisez convenablement la commande `ls` sur le répertoire `/run/user/<votre_UID>/gvfs/` pour vérifier que le démontage a réussi.

## → Observation du résultat

**{-à faire-}** Consultez votre espace Nextcloud via un navigateur web.
Est-ce que les manipulations que vous venez de faire sont présentes ?

**{-à faire-}** Consultez les messages envoyés dans votre messagerie Zimbra.
Est-ce que le courriel que vous venez d'envoyer au responsable de votre groupe
est bien présent ? Contient-il l'archive ZIP en pièce jointe ? Est-ce que
l'archive ZIP contient bien tous les sous-répertoires et fichiers que vous
avez créés au terminal dans votre espace Nextcloud ?
