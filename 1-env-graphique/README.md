> # Prise en main de l'environnement de travail
> [Outils pour le développement informatique](../Readme.md) \
> 1re année de licence maths-info, Univ. Lille \
> septembre 2023 \
> [![](https://mirrors.creativecommons.org/presskit/icons/cc.png){width=20}![](https://mirrors.creativecommons.org/presskit/icons/by.png){width=20}](https://creativecommons.org/licenses/by/4.0/deed.fr) 


# 1. Objectifs

Ce document vise à vous faire découvrir l'environnement de travail pour travailler vos TDM de 
l'UE Programmation durant tout le premier semestre, mais aussi le semestre prochain.

Vous découvrirez les logiciels et sites que vous serez amenés à utiliser chaque semaine.

La présentation est faite selon la configuration des postes de
travail des salles de TP des bâtiments SUP et M5. Mais tout ce qui est décrit ci-après peut aisément
être transposé à vos ordinateurs personnels.


# 2. Utiliser un poste de travail des bâtiments SUP et M5

Comme pour tout poste de travail de l'université, leur utilisation impose une identification.

Les ordinateurs des salles de TP du SUP et du M5 fonctionnent avec un **système d'exploitation**
Linux.

## → Se connecter

Au début d'une séance de TP, vous utilisez votre identifiant et mot de passe pour vous y connecter.

**{+attention+}** L'identifiant est de la forme `prenom.nom.etu`. Il s'agit du début de votre adresse
de messagerie, mais sans la partie domaine (pas de `@univ-lille.fr`).

Une fois connecté, l'ordinateur affichera votre environnement de travail : 

![environnement](./img/environnement.png)


## → Se déconnecter

En fin de séance, il faudra vous déconnecter. Pour cela :

- cliquez sur l'icône apparaissant en haut à droite du bureau ;
- puis sur votre nom ;
- et enfin cliquez sur l'option `Logout`.

![déconnexion](./img/deconnexion.png)

**{-à faire-}** Déconnecter le membre connecté du binôme, puis connecter l'autre.

# 3. Ouvrir des applications

L'environnement dispose d'une barre d'icônes permettant d'exécuter certaines applications, mais pas
toutes.

**{-à faire-}** Identifier les applications de la barre d'icône en les survolant à l'aide de la souris.


Les autres applications peuvent être exécutées en utilisant le bouton situé en haut, à gauche.


Ce bouton permet de lancer une recherche parmi les applications :

![menus-application](./img/menu-applications.png)

**{+remarque+}** La recherche peut être lancé en utilisant le raccourci clavier <kbd><img src="./img/winlogo.png" alt="logo window"/></kbd>.


**{-à faire-}** En utilisant la recherche, lancer le **gestionnaire de fichiers** (le nom de
l'application est `Files`)

**{+remarques+}** 

1. Le gestionnaire de fichiers peut être lancé de plusieurs manières :

- en effectuant une *recherche* ;
- en utilisant l'icône correspondante dans la *barre d'icônes* ;
- en double-cliquant sur l'*icône du bureau* représentant un dossier contenant une maison.

2. Le gestionnaire de fichiers permet d'accéder à vos fichiers enregistrés sur la machine et à les
   manipuler (déplacement, copie, création...).
3. Par défaut, le gestionnaire de fichiers affiche votre **dossier personnel** (ou répertoire `HOME`)  
 
## → Copie d'écran

Une partie de l'évaluation de l'UE ODI se fera par le dépôt de copies d'écran.

Pour effectuer une copie du bureau, il suffit d'utiliser la touche <kbd>Impr Écran</kbd> ou
<kbd>Print Screen</kbd> (ou équivalent) du clavier. Cette touche est située en haut à droite du
clavier. Lorsque vous appuyez puis relâchez cette touche, une copie du bureau est générée, mais elle
n'est pas automatiquement affichée.

Les fichiers générés par cette commande se trouvent dans le dossier `Pictures` de votre dossier
personnel.

**{-à faire-}** Utiliser le gestionnaire de fichiers pour ouvrir votre copie d'écran et laisser les deux
fenêtres ouvertes : celle du gestionnaire de fichiers et celle de l'application avec laquelle l'image
a été ouverte.

Pour les premières séances, effectuez des copies d'écran pour attester du travail effectué et
conservez les précieusement dans votre répertoire personnel. Nous vous demanderons plus tard de les
déposer dans un dépôt git.

## → Manipulation des fenêtres

Dans les UE d'informatique, vous serez souvent amené·es à utiliser plusieurs fenêtres lors des TDM. Il
faut savoir rapidement redimensionner vos fenêtres, passer d'une fenêtre à l'autre en utilisant la souris et le clavier.

### Passer d'une fenêtre à l'autre

Le raccourci clavier pour passer d'une fenêtre à l'autre est la combinaison
<kbd>Alt</kbd>+<kbd>TAB</kbd>. La touche <kbd>Alt</kbd> est située à gauche de la barre espace, la
touche de tabulation <kbd>TAB</kbd> est située à gauche de la touche <kbd>A</kbd> sur les claviers
français, son dessin symbolise deux flèches horizontales de sens contraires.

**{-à faire-}** Utiliser la combinaison de touche <kbd>Alt</kbd>+<kbd>TAB</kbd> pour changer
l'application au premier plan.

**{+remarque+}** Lorsque vous avez plus de deux applications ouvertes, les combinaisons de touches
<kbd>Alt</kbd>+<kbd>TAB</kbd> et <kbd>Alt</kbd>+<kbd>shift</kbd>+<kbd>TAB</kbd> permettent
respectivement d'avancer et de reculer dans la liste des fenêtres ouvertes (la touche
<kbd>Shift</kbd> se situe au dessus de la touche <kbd>Ctrl</kbd>).

### Redimensionner des fenêtres

#### En utilisant les contrôles du gestionnaire de fenêtres

Le gestionnaire de fenêtres décore les fenêtres des applications avec des boutons permettant d'agir
sur l'aspect de celles-ci :

![controles-fenetre](./img/controles-fenetre.png)


Ces trois boutons permettent respectivement de réduire, maximiser et fermer la fenêtre.

Lorsqu'une fenêtre est réduite, on peut la ré-afficher soit en cliquant sur son icône dans la barre
d'icônes, soit en utilisant le raccourci <kbd>Alt</kbd>+<kbd>TAB</kbd>.

Lorsqu'une fenêtre est maximisée, on peut restaurer son état en utilisant le même bouton.

**{-à faire-}** Réduire une fenêtre, la restaurer, la maximiser et enfin la restaurer.

**{+remarque+}** Vous pouvez retrouver ces opérations en utilisant le menu accessible par un clic droit
(c'est-à-dire en utilisant le bouton de droite de la souris) sur la barre d'état de la fenêtre :

![menu-fenetre](./img/menu-fenetre.png)

#### En utilisant les raccourcis clavier

Un·e programmeur·e efficace évite les déplacements inutiles de ses mains et préfère utiliser les
raccourcis clavier pour manipuler la géométrie des fenêtres. Voici certains raccourcis clavier
agissant sur les fenêtres :

| Raccourci                                                                       | Effet                                                   |
|---------------------------------------------------------------------------------|---------------------------------------------------------|
| <kbd><img src="./img/winlogo.png" alt="logo window"/></kbd> + <kbd>Haut</kbd>   | Maximiser la fenêtre                                    |
| <kbd><img src="./img/winlogo.png" alt="logo window"/></kbd> + <kbd>Bas</kbd>    | Restaurer la géométrie de la fenêtre                    |
| <kbd><img src="./img/winlogo.png" alt="logo window"/></kbd> + <kbd>Droite</kbd> | Maximiser à droite (occupe la moitié droite de l'écran) |
| <kbd><img src="./img/winlogo.png" alt="logo window"/></kbd> + <kbd>Gauche</kbd> | Maximiser à gauche (occupe la moitié gauche de l'écran) |

**{+remarques+}** 

1. Les touches <kbd>Haut</kbd>, <kbd>Bas</kbd>, <kbd>Gauche</kbd> et
<kbd>Droite</kbd> sont appelées touches de direction et sont représentées par une flèche indiquant
la direction.
2. La séparation de l'espace de travail en deux vous sera utile pour, par exemple, afficher votre éditeur et le
   sujet du TP en même temps.

**{-à faire-}** Ouvrir deux fenêtres, en placer une à gauche et l'autre à droite.

**{+pour aller plus loin+}** Lire la [mini-BD Dis... c'est quoi... ?](https://arpinux.org/discestquoi/) dédiée : [Dis.. c'est quoi... un raccourci clavier ?](https://arpinux.org/discestquoi/dcq/dcq-clavier.pdf)


## → Ouvrir un shell

Le shell ou interpréteur en ligne de commande est un programme permettant de dialoguer avec le
système de manière textuelle : on y écrit des commandes pour qu'elles soient
exécutées. L'utilisation du shell fera l'objet d'un prochain TP.

Le système installé dispose souvent de plusieurs shell.

**{-à faire-}** Effectuer une recherche d'applications en utilisant le mot-clé `shell` et identifier les différents shell graphiques disponibles.

Le shell `Tilix` peut être lancé de plusieurs manières :

- via le menu des applications ;
- via la barre d'icônes ;
- en utilisant le raccourci clavier <kbd>Ctrl</kbd>+<kbd>Alt</kbd>+<kbd>T</kbd>.

**{-à faire-}** Lancer `Tilix` en utilisant le raccourci clavier.

![Tilix](./img/tilix.png)

## → Ouvrir le navigateur web

Le système installé dispose d'au moins deux navigateurs web :

- le navigateur de Google chrome ;
- le navigateur Firefox.

**{-à faire-}** Lancer le navigateur Firefox.

Par défaut, le navigateur affiche au démarrage la page de l'intranet du FIL. Le FIL est la
composante de l'université en charge des formations en informatique.

**{-à faire-}** Cliquer sur le lien `Liens utiles` et identifier les services accessibles. Laissez ouverte l'application.

Par défaut, le menu de l'application `Firefox` est caché, la touche <kbd>Alt</kbd> permet de le
faire apparaître. Cela peut être utile pour accéder aux fonctions du logiciel.

## → Ouvrir un éditeur de texte

Parfois il vous sera demandé de créer ou d'ouvrir un fichier texte pour y saisir des
informations. Le code source de vos programmes écrits dans le langage Python sera édité à l'aide d'un éditeur spécifique. Pour tous les autres fichiers, vous pouvez utiliser l'éditeur `gedit` en effectuant une recherche dans le menu des applications (entrez `gedit` ou `Text ...`)

**{-à faire-}** Écrire les noms et prénom du binôme dans l'éditeur.


Pour sauvegarder un document dans un fichier, vous pouvez utiliser le menu de l'application ou mieux en utilisant le raccourci clavier <kbd>Ctrl</kbd>+<kbd>S</kbd>

**{-à faire-}** En utilisant le raccourci clavier, sauvegarder le document dans un fichier nommé
`odi-tp1-NOM1-NOM2.txt` où `NOM1` et `NOM2` sont les noms des étudiants du binôme. Laisser ouverte l'application.


Les éditeurs de texte partagent souvent les mêmes raccourcis. Voici une table des raccourcis du menu `Fichier` :

| Raccourci                    | Action                   |
|------------------------------|--------------------------|
| <kbd>Ctrl</kbd>+<kbd>N</kbd> | Crée un nouveau document |
| <kbd>Ctrl</kbd>+<kbd>O</kbd> | Ouvre un fichier         |
| <kbd>Ctrl</kbd>+<kbd>S</kbd> | Sauvegarde un document   |
| <kbd>Ctrl</kbd>+<kbd>W</kbd> | Ferme un document        |
| <kbd>Ctrl</kbd>+<kbd>Q</kbd> | Quitte l'application     |

## → Copier-coller

En tant que programmeur·e, on est souvent amené·e à manipuler du texte. Les opérations de Copier-coller sont à la base de ces manipulations.

Trois manipulations sont nécessaires :

- la sélection ;
- la copie ou la coupe (copie accompagnée d'un effaçage);
- le collage.

### En utilisant la souris

1. La **sélection** s'effectue en maintenant le bouton gauche de la souris et en survolant le texte à sélectionner. 
2. Un clic droit sur le texte sélectionné permet d'afficher un menu dit **contextuel** présentant les opérations possibles à cet endroit. L'entrée `Copier` permet de copier le texte dans le **presse-papier**.
3. Dans une autre application ou à un autre endroit, on utilise aussi le menu contextuel et l'entrée `Coller` pour insérer le texte du presse-papier dans l'application.

**{+remarque+}** Il est possible de sélectionner un mot entier en double cliquant (2 clics rapides) dessus, puis une ligne entière en cliquant une troisième fois juste après.

### En utilisant les raccourcis clavier

1. La sélection s'effectue en maintenant la touche <kbd>Shift</kbd> enfoncée, puis en déplaçant le curseur à l'aide des touches de direction. 
2. La copie s'effectue en utilisant le raccourci clavier <kbd>Ctrl</kbd>+<kbd>C</kbd> ; la coupe s'effectue en utilisant le raccourci clavier <kbd>Ctrl</kbd>+<kbd>X</kbd> .
3. Le collage s'effectue en utilisant le raccourci clavier <kbd>Ctrl</kbd>+<kbd>V</kbd>.

**{+remarques+}** 

- Il est possible de déplacer le curseur au début d'une ligne à l'aide de la touche <kbd>↖</kbd> ou <kbd>Home</kbd> et à la fin d'une ligne à l'aide de la touche <kbd>Fin</kbd> ou <kbd>End</kbd>. 
Si vous maintenez la touche <kbd>Shift</kbd> enfoncée en utilisant ces raccourcis, vous sélectionnerez plus rapidement une longue portion de texte.

- Le raccourci <kbd>Ctrl</kbd>+<kbd>A</kbd> sélectionne l'ensemble du contenu d'une fenêtre.


**{-à faire-}** En utilisant la souris, sélectionner les textes des liens utiles présents dans le navigateur, puis **utiliser les raccourcis clavier** pour :

- copier le texte dans le presse-papier ;
- accéder à l'éditeur de texte ;
- coller le texte dans l'éditeur.
- enregistrer le fichier


## → Ouvrir Thonny

`Thonny` est un environnement de développement permettant d'écrire des programmes dans le langage
`Python`.

Le langage `Python` est celui choisi pour les UE de programmation de la première année.

Sur les ordinateurs des bâtiments SUP et M5, deux versions de `Thonny` sont installées :

- la version basique désignée simplement par `Thonny` ;
- une version enrichie d'un environnement de test appelée `Thonny-l1test`.

Cette année, nous utiliserons cette deuxième version.

**{-à faire-}** En utilisant le formulaire de recherche des applications, lancer `thonny-l1test`.

Au premier démarrage, le programme installe les fichiers nécessaires dans votre répertoire de travail **ce qui peut prendre plusieurs minutes**, ne paniquez pas !

![thonny-attente](./img/thonny-attente.png)

Puis un accord sur la collecte des données recueillies à travers l'application vous est demandé :

![thonny-trace](./img/thonny-trace.png)

**{+pour aller plus loin+}** Sur la collecte de donnes réalisée par L1log, 
lire la [page dédiée](https://www.fil.univ-lille.fr/~L1S1Info/MI/collecte_donnees_thonny.pdf).

Enfin le logiciel se lance :

![thonny](./img/thonny.png)

Thonny fera l'objet d'une prise en main dans un TP spécifique.


